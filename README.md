## Test

```bash
TEST_ZONE_NAME=k8s.cm. make test
```

## Build

```bash
docker buildx build --platform linux/amd64 -t cert-manager-webhook-pinto .
```

## Push

```bash
docker tag cert-manager-webhook-pinto wearep3r/cert-manager-webhook-pinto
docker push wearep3r/cert-manager-webhook-pinto
```

## Install

```bash
helm upgrade --install cert-manager-webhook-pinto ./deploy/cert-manager-webhook-pinto --namespace pinto --set certManager.namespace=pinto --set image.repository=registry.gitlab.com/p3r.one/cert-manager-webhook-pinto --set groupName=acme.pinto.io
```